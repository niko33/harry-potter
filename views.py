
import requests


def home():
    response = requests.get('https://api.tfl.gov.uk/Journey/JourneyResults/51.511057060999995%2C%20-0.11420063834000001/'
                            'to/51.5098291974%2C%20-0.07679704678?nationalSearch=false&app_id=8507abfe&app_key=8774c2541b26d27fde40c45230c78989')
    journey = response.json()

    return journey

print(home())
