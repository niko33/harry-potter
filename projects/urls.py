from django.urls import path
from projects import views

urlpatterns = [
    path('', views.get_form, name='project'),
    path('obiquities', views.print_departures, name='project'),
    path('possibleArrival', views.print_arrivals, name='project'),
    path('possibleJourneys', views.print_itineraries, name='project'),
    path('journeyDetail', views.print_details, name='project'),
]
