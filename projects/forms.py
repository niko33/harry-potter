from django import forms

class NameForm(forms.Form):
    departure = forms.CharField(label='Departure', max_length=100)
    arrival = forms.CharField(label='Arrival', max_length=100)