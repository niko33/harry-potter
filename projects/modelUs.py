from django.db import models
import requests
import json

#---DIGA

class infoLocation():
    def __init__(self, name, lat, long):
        self.name = name
        self.lat = lat
        self.long = long

def compute_url(location_from, location_to):
    s = "https://api.tfl.gov.uk/Journey/JourneyResults/{}/to/{" \
        "}?app_id=8507abfe&app_key=8774c2541b26d27fde40c45230c78989%09".format(location_from, location_to)
    return s

def request_json(url):
    response = requests.get(url)
    journey = response.json()
    return journey

def get_disambiguations(journey):
    disambiguation = []
    try:
        disamb_to = journey['toLocationDisambiguation']
        disambiguation.append(disamb_to)
    except:
        pass

    try:
        disamb_from = journey['fromLocationDisambiguation']
        disambiguation.append(disamb_from)
    except:
        pass

    return disambiguation

def get_disambiguations_location(disambiguation_list):
    n_child = len(disambiguation_list['disambiguationOptions'])
    if n_child > 5:
        n_child= 5
    option_list = []
    for i in range(n_child):
        name = disambiguation_list['disambiguationOptions'][i]['place']['commonName']
        lat = disambiguation_list['disambiguationOptions'][i]['place']['lat']
        long = disambiguation_list['disambiguationOptions'][i]['place']['lon']
        option_list.append(infoLocation(name, lat, long))
    return option_list

def chooseLocations(start, arrival):
    url = compute_url(start, arrival)
    request = request_json(url)
    disambiguations = get_disambiguations(request)
    return ( get_disambiguations_location(disambiguations[0]) , get_disambiguations_location(disambiguations[1]))

#-----------------------------------------------------------------------------------------

class JourneyPreview():
    def __init__(self, startDateTime, arrivalDateTime, duration, modes, totalCost, walkingDistance):
        self.startDateTime = startDateTime
        self.arrivalDateTime = arrivalDateTime
        self.duration = duration
        self.modes = modes
        self.totalCost = totalCost
        self.walkingDistance = walkingDistance
        self.string = 'Estimated arrival: {}, Journey duration: {}, Travel with: {}, Total cost: {} £, Walking Distance: {}'.format(self.arrivalDateTime, self.duration, self.modes, self.totalCost/100, self.walkingDistance)


def getJourneys(fromGPS, toGPS):
    response = requests.get(
        'https://api.tfl.gov.uk/Journey/JourneyResults/{}%2C{}/to/{}%2C{}?nationalSearch=false&app_id=8507abfe&app_key=8774c2541b26d27fde40c45230c78989'
        .format(fromGPS[0], fromGPS[1], toGPS[0], toGPS[1]))
    journeysList = response.json()['journeys']
    journeyPreviews = []
    for journey in journeysList:

        try:
            fare = journey['fare']['totalCost']
        except KeyError:
            fare = 0

        modes = []
        walkingDistance = 0
        for leg in journey['legs']:
            mode = leg['mode']['name']
            if mode not in modes:
                modes.append(mode)
            try:
                walkingDistance = walkingDistance + leg['distance']
            except KeyError:
                pass

        journeyPreview = JourneyPreview(journey['startDateTime'], journey['arrivalDateTime'], journey['duration'],
                                        modes, fare, walkingDistance)
        journeyPreviews.append(journeyPreview)

    return journeyPreviews


def compute_url_GPS(from_lat, from_long, to_lat, to_long):
    s = "https://api.tfl.gov.uk/Journey/JourneyResults/{}/to/{" \
        "}?app_id=8507abfe&app_key=8774c2541b26d27fde40c45230c78989"\
        .format(from_lat+"%2c"+from_long, to_lat+"%2c"+to_long)
    return s

def get_selected_journey(json, index):
    return json['journeys'][index]

class detailedJourney():
    def __init__(self, from_lat, from_long, to_lat, to_long, journey_index):
        self.from_lat = from_lat
        self.from_long = from_long
        self.to_lat = to_lat
        self.to_long = to_long
        self.journey_index = journey_index
        self.compute_travel()
        self.string = self.give_summary()

    def compute_travel(self):
        url = compute_url_GPS(self.from_lat, self.from_long, self.to_lat, self.to_long)
        self.journey_to_display = get_selected_journey(request_json(url), self.journey_index)

    def give_summary(self):
        summary = []
        summary.append("You will arrive in: " + str(self.journey_to_display['duration']) + " minutes")
        s = str(self.journey_to_display['arrivalDateTime']).rpartition("T")[2]
        summary.append("Arrival time: " + s)

        summary.append("road to take:")
        summ = ""
        n_step = len(self.journey_to_display['legs'])
        for i in range(0, n_step):
            if(self.journey_to_display['legs'][i]['mode']['name'] == "bus"):
                summ += "- take the " + self.journey_to_display['legs'][i]['mode']['name'] + " for "
            elif(self.journey_to_display['legs'][i]['mode']['name'] == "tube"):
                summ += "- take the " + self.journey_to_display['legs'][i]['mode']['name'] + " for "
            else:
                summ += "- keep " + self.journey_to_display['legs'][i]['mode']['name'] + " for "
            summ += str(self.journey_to_display['legs'][i]['duration']) + " minute"
            summ += " toward " + self.journey_to_display['legs'][i]['instruction']['summary'] + "\n"
            summary.append(summ)
            summ=""
        try:
            summary.append("Total cost: " + str(self.journey_to_display['fare']['totalCost']/100) + " £")
        except:
            pass

        return summary



