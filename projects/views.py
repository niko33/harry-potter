from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import NameForm
from .modelUs import chooseLocations, getJourneys, detailedJourney
# Create your views here.


def get_form(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = NameForm(request.POST)

        # check whether it's valid:
        if form.is_valid():
            departure = form.cleaned_data['departure']
            arrival = form.cleaned_data['arrival']
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            return HttpResponseRedirect('/obiquities?departure={}&arrival={}'.format(departure, arrival))

    # if a GET (or any other method) we'll create a blank form
    else:
        form = NameForm()

    return render(request, 'journey.html', {'form': form})


def print_departures(request):

    list = chooseLocations(request.GET.get('departure'), request.GET.get('arrival'))

    return render(request, 'obiquities.html', {'departures': list[1], 'arrival': request.GET.get('arrival')})

def print_arrivals(request):

    list = chooseLocations(request.GET.get('departure'), request.GET.get('arrival'))
    d_lat = request.GET.get('d_lat')
    d_long = request.GET.get('d_long')

    return render(request, 'possibleArrival.html', {'arrivals':list[0], 'd_lat': d_lat, 'd_long': d_long})

def print_itineraries(request):

    list = getJourneys( (request.GET.get('d_lat'), request.GET.get('d_long') ), ( request.GET.get('a_lat'), request.GET.get('a_long')))
    d_lat = request.GET.get('d_lat')
    d_long = request.GET.get('d_long')
    a_lat = request.GET.get('a_lat')
    a_long = request.GET.get('a_long')

    return render(request, 'possibleJourneys.html', {'journeys': list, 'd_lat': d_lat, 'd_long': d_long, 'a_lat': a_lat, 'a_long': a_long})


def print_details(request):
    d_lat = request.GET.get('d_lat')
    d_long = request.GET.get('d_long')
    a_lat = request.GET.get('a_lat')
    a_long = request.GET.get('a_long')
    index = int(request.GET.get('i'))

    details = detailedJourney(d_lat, d_long, a_lat, a_long, index)

    return render(request, 'journeyDetail.html', {'details': details.string})